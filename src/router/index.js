import Vue from 'vue'
import Router from 'vue-router'
import Graphs from '@/components/Graphs'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Graphs',
      component: Graphs
    }
  ]
})
